'use strict';

var $ = require('jquery');
var io = require('socket.io-browserify');
var fs = require('fs');
var _ = require('lodash');

var messages = require('./messages');

var messageTpl = fs.readFileSync(__dirname + '/../../templates/message.html');

/**
 * preprocess mail object before rendering
 *
 * @param {Object} message
 * @returns {Object}
 */
var messageFieldsTransform = function (message) {
    var text;

    if (message.text) {
        text = message.text.replace(/\n/gi, '<br>');
    } else {
        text = message.html;
    }

    var result = {
        subject: message.subject,
        seqIndex: message.seqIndex,
        text: text,
        from: '(' + message.from[0].name + ') ' + message.from[0].address,
        date: message.date.replace('T', ' ')
    };

    return result;
};

/**
 * creates html template from a list of emails
 *
 * @param {Array} messages
 * @returns {*|string|String}
 */
var createMessagesHtml = function (messages) {
    var sortedMessages = messages.sort(function (msgA, msgB) {
        return msgB.seqIndex - msgA.seqIndex;
    });

    var messageHtml = _.map(sortedMessages, function (msg) {
        return  _.template(messageTpl, messageFieldsTransform(msg));
    }).join('');

    return messageHtml;

};

/**
 * update paginator
 * @param from
 * @param to
 * @param total
 */
var updatePaginator = function (from, to, total) {
    var $toArrow = $('#to-arrow');
    var $fromArrow = $('#from-arrow');
    var $from = $('#from');
    var $to = $('#to');

    $from.text(from);
    $to.text(to);

    if (to === total) {
        $toArrow.removeClass('active-link');
    } else if (!$toArrow.hasClass('active-link')) {
        $toArrow.addClass('active-link');
    }

    if (from === 0) {
        $fromArrow.removeClass('active-link');
    } else if (!$fromArrow.hasClass('active-link')) {
        $fromArrow.addClass('active-link');
    }
};

$(function () {

    var socket = io.connect('http://localhost');

    socket.on('connect', function () {
        console.log('connected');
    });

    var STEP = 20;
    var totalMessages;

    $('#loader').removeClass('hide');

    // get last messages on start
    messages.getTotal(socket)
        .then(function (total) {

            var from = total - STEP;
            if (from <= 0) {
                from = 1;
            }
            var to = total;
            totalMessages = total;

            $('#paginator').removeClass('hide');
            updatePaginator(from, to, total);

            return messages.fetch({from: from, to: to}, socket);
        })
        .then(function (messages) {
            var messageHtml = createMessagesHtml(messages);

            $('#loader').addClass('hide');
            $('#container').append(messageHtml);
        })
        .fail(function (error) {
            console.log(error);
        });


    // load messages on paginator click
    $('#paginator').on('click', '#from-arrow.active-link, #to-arrow.active-link', function (event) {
        var from = Number($('#from').text());
        var to = Number($('#to').text());

        if ($(this).hasClass('from')) {
            from -= STEP;
            to -= STEP;
        } else {
            from += STEP;
            to += STEP;
        }

        if (from <= 0) {
            from = 1;
        }

        $('#loader').removeClass('hide');

        messages.fetch({from: from, to: to}, socket)
            .then(function (messages) {
                console.log('messages', messages);
                var messageHtml = createMessagesHtml(messages);
                $('#loader').addClass('hide');
                updatePaginator(from, to, totalMessages);
                $('#container').html(messageHtml);
            });
    });

});