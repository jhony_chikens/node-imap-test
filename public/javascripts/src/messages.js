'use strict';

var Q = require('q');

exports.getTotal = function (socket) {
    var deferred = Q.defer();

    socket.emit('messages:total');

    socket.on('total:result', function (total) {
        deferred.resolve(total);
    });
    socket.on('total:error', function (error) {
        deferred.reject(error);
    });
    return deferred.promise;
};


exports.fetch = function (options, socket) {
    var deferred = Q.defer();

    var from = options.from;
    var to = options.to;

    var messages = [];

    socket.emit('messages:fetch', options);

    socket.on('fetch:error', function (error) {
        deferred.reject(error);
    });

    socket.on('fetch:result', function (message) {
        var index = message.seqIndex;

        messages.push(message);

        if (to === index) {
            deferred.resolve(messages);
        }
    });

    return deferred.promise;
};