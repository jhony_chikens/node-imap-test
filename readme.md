### TEST Project That gets all emails form inbox folder using IMAP

In order to start just clone it and specify your email credentials in config.js file then:

    npm install
    npm start

## Technology that was used

Backend:
    express.io
    sockets.io
    node-imap

Frontend:
    browserify
    jquery

Everywhere:
    lodash
    Q

