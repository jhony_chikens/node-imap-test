'use strict';

module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        browserify: {
            dist: {
                files: {
                    'public/javascripts/build/bundle.js': ['public/javascripts/src/**/*.js']
                },
                options: {
                    transform:  ['brfs']
                }
            }
        },
        watch: {
            scripts: {
                files: ['public/javascripts/src/**/*.js', 'public/templates/*.html'],
                tasks: ['browserify'],
                options: {
                    spawn: false
                }
            }
        }
    });


    // Default task(s).
    grunt.registerTask('default', ['watch']);


};