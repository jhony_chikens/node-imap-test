'use strict';
var Imap = require('imap');
var MailParser = require("mailparser").MailParser;
var util = require('util');
var Q = require('q');

var config = require('../config');

var imapInstance;

/**
 * Wrapper for node-imap
 * @returns {*}
 * @constructor
 */
var ImapBroker = function () {
    if (imapInstance) {
        return imapInstance;
    }

    this.imap = new Imap(config.imap);
    imapInstance = this;
};

/**
 * open inbox
 * @returns {promise|defer.promise|Q.promise}
 */
ImapBroker.prototype.openInbox = function () {
    var readOnly = true;
    var deferred = Q.defer();
    this.imap.openBox('INBOX', readOnly, deferred.makeNodeResolver());

    return deferred.promise;

};

/**
 * Fetch messages by range
 * returns fetch event emitter
 *
 * @param options
 * @returns {*}
 */
ImapBroker.prototype.fetch = function (options) {
    var from = String(options.from) || '*';
    var to = String(options.to) || '*';
    var fields = options.fields || {};

    var range = util.format('%s:%s', from, to);
    return this.imap.seq.fetch(range, fields);
};

/**
 * connect to imap server
 * @returns {promise|defer.promise|Q.promise}
 */
ImapBroker.prototype.connect = function () {
    var deferred = Q.defer();

    this.imap.once('ready', function () {
        deferred.resolve();
    });

    this.imap.once('error', function (err) {
        console.log('[ImapBroker.prototype.connect]', err);
        deferred.reject(new Error(err));
    });
    this.imap.connect();

    return deferred.promise;
};

/**
 * parse mail body and header using mailParser
 *
 * @param stream
 * @returns {promise|defer.promise|Q.promise}
 */
ImapBroker.parseMessage = function (stream) {
    var deferred = Q.defer();
    var mailParser = new MailParser({streamAttachments: true});

    mailParser.on('end', function (mail) {
        deferred.resolve(mail);
    });

    stream.on('data', function(chunk){
        mailParser.write(chunk.toString());
    });

    stream.on('end', function(){
        mailParser.end();
    });

    return deferred.promise;
};

module.exports = ImapBroker;

