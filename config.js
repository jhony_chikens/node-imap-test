'use strict';


module.exports = {
    imap: {
        user: 'xxxxx@xxxx.com',
        password: 'xxxxx',
        host: 'imap.xxx.com',
        port: 993,
        tls: true,
        tlsOptions: {
            rejectUnauthorized: false
        }
    }
};