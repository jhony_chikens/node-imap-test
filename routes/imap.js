'use strict';
var Q = require('q');


var ImapBroker = require('../lib/imapBroker');

/**
 * get total amount of messages in inbox
 * @param req
 */
exports.getTotal = function (req) {
    var imapBroker = new ImapBroker();

    imapBroker.connect()
        .then(imapBroker.openInbox.bind(imapBroker))
        .then(function (box) {
            console.log('TOTAL:RESULT', box.messages.total);

            imapBroker.imap.once('end', function () {
                console.log('total:res:end', box.messages.total);
                req.io.emit('total:result', box.messages.total);
            });

            imapBroker.imap.once('error', function (error) {
                console.log('total:res:error', box.messages.total);
                if (box.messages) {
                    req.io.emit('total:result', box.messages.total);
                }
            });

            imapBroker.imap.end();
        })
        .fail(function (error) {
            req.io.emit('total:error', error);
        });
};


/**
 * fetch messages from inbox by range
 *
 * each message emits on client using sockets separately
 *
 * @param req
 */
exports.fetchMessages = function (req) {
    var from = req.data.from;
    if (from <= 0) {
        from = 1;
    }
    var to = req.data.to;
    var imapBroker = new ImapBroker();

    imapBroker.connect()
        .then(function () {
            console.log(from, to);
            return imapBroker.openInbox();
        })
        .then(function () {
            var deferred = Q.defer();
            var fetchEmitter = imapBroker.fetch({
                from: from,
                to: to,
                fields: {
                    bodies: '',
                    struct: true
                }
            });

            fetchEmitter.once('error', function (error) {
                console.log('fetching:error');
                deferred.reject(error);
            });
            fetchEmitter.once('end', function () {
                console.log('fetching:end');
                deferred.resolve();
            });

            fetchEmitter.on('message', function (msg, index) {
                console.log('message:index', index);

                msg.on('body', function (stream) {
                    ImapBroker.parseMessage(stream).then(function (mail) {
                        mail.seqIndex = index;
                        req.io.emit('fetch:result', mail);
                    });
                });
            });
            return deferred.promise;
        })
        .fail(function (error) {
            req.io.emit('fetch:error', error);
        })
        .fin(function () {
            console.log('END');
            imapBroker.imap.end();
        });
};