'use strict';

/**
 * Module dependencies.
 */

var express = require('express.io');
var routes = require('./routes');
var imap = require('./routes/imap');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' === app.get('env')) {
    app.use(express.errorHandler());
}

app.get('/', routes.index);
app.http().io();

app.io.route('messages:fetch', imap.fetchMessages);
app.io.route('messages:total', imap.getTotal);

app.listen(app.get('port'));
console.log('Express server listening on port ' + app.get('port'));

